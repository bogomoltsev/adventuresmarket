# This file is a template, and might need editing before it works on your project.
FROM python:3

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
COPY . /usr/src/app

RUN apt-get update \
    && apt-get install -y gunicorn3 \
    && rm -rf /usr/src/app/static

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip3 install --no-cache-dir -r requirements.txt

# For Django
EXPOSE 8000
CMD ["gunicorn3", "--log-level","debug","--bind", "0.0.0.0:8000","printerio.wsgi:application"]

# For some other command
# CMD ["python", "app.py"]
