// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'
import navbar from '@/components/Navbar'
import footerSection from '@/components/Footer'

Vue.config.productionTip = false
Vue.use(Buefy)
Vue.component('navbar', navbar)
Vue.component('footerSection', footerSection)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
