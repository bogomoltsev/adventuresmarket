import { HTTP } from './api'

export const Index = {
  list () {
    return HTTP.get('/index/').then(response => {
      return response.data
    })
  }
}