from django.urls import path, re_path
from b2c import views
from django.conf.urls import url
from django.contrib.auth.decorators import login_required, permission_required

#@login_required
#@permission_required('b2c.is_user')

urlpatterns = [
    url(r'^api/index/', views.index),
    url(r'^api/user/(.)*$', views.user_area),
]