from django.forms import ModelForm
from .models import ClientItem

class AddItemF(ModelForm):
    class Meta:
        model=ClientItem
        fields = ['item','quantity']
