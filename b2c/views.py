from django.http import HttpResponse
from django.shortcuts import render
from rest_framework.decorators import api_view
from django.views import View
from django.contrib.auth.decorators import login_required, permission_required
from .models import ClientItem
# Create your views here.
#@method_decorator(login_required)
#@method_decorator(login_required, name='dispatch')

@api_view(['GET'])
def index(request):
    template_name='index.html'
    return render(request, self.template_name ,{})

@api_view(['GET'])
def user_area(request):
    return HttpResponse(status=200)
