from django.db import models
from django.contrib.auth.models import User, Group, Permission, AbstractUser

class UserAccount(models.Model):
    class Meta:
        permissions = (
            ("is_user", "Can add cars"),
        )

class Items(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=128)
    manufacturer = models.CharField(max_length=128)
    def __str__(self):
        return self.name

class Cartriges(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=128)
    items = models.ManyToManyField(Items)

class Supplier(models.Model):
    id = models.AutoField(primary_key=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

class SuplierItem(models.Model):
    DEFAULT=1
    id = models.AutoField(primary_key=True)
    item = models.OneToOneField(Items, on_delete=models.CASCADE)
    suplier = models.OneToOneField(Supplier, on_delete=models.CASCADE)

class Client(models.Model):
    id = models.AutoField(primary_key=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True)
    birth_date = models.DateField(null=True, blank=True)

class ClientItem(models.Model):
    DEFAULT=1
    id = models.AutoField(primary_key=True)
    item = models.OneToOneField(Items, on_delete=models.CASCADE)
    client = models.OneToOneField(Client, on_delete=models.CASCADE)
    quantity= models.IntegerField(default=1)

