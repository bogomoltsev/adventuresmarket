from django.contrib import admin
from .models import Items, Client, Supplier, ClientItem, SuplierItem


admin.site.register(Client)
admin.site.register(Supplier)
admin.site.register(ClientItem)
admin.site.register(SuplierItem)
admin.site.register(Items)

# Register your models here.
